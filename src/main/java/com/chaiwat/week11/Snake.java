package com.chaiwat.week11;

public class Snake extends Animal implements Crawlable{

    public Snake(String name) {
        super(name, 0);
        
    }

    @Override
    public void craw() {
        System.out.println(this.toString() + " craw.");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
        
    }

    @Override
    public String toString() {
        return "Snake(" + this.getName() + ")";
    }
}
