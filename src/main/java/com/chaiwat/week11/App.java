package com.chaiwat.week11;

public class App {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Badman");
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.landing();
        bat1.takoff();

        System.out.println(' ');

        Bird bird1 = new Bird("Nok");
        bird1.eat();
        bird1.sleep();
        bird1.fly();
        bird1.landing();
        bird1.takoff();

        System.out.println(' ');

        Fish fish1 = new Fish("Salmon");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        System.out.println(' ');

        Submarine submarine1 = new Submarine("Submadi","Submadi77 Engine");
        submarine1.swim();

        System.out.println(' ');

        Plane plane1 = new Plane("Boeing", "Boeing77 Engine");
        plane1.takoff();
        plane1.fly();
        plane1.landing();

        System.out.println(' ');

        Crocodile crocodile1 = new Crocodile("Fedfe");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        crocodile1.craw();

        System.out.println(' ');

        Snake snake1 = new Snake("Viper");
        snake1.eat();
        snake1.sleep();
        snake1.craw();

        System.out.println(' ');

        Rat rat1 = new Rat("jerry");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        System.out.println(' ');

        Cat cat1 = new Cat("Tom");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();

        System.out.println(' ');

        Dog dog1 = new Dog("Spike");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();

        System.out.println(' ');

        Human human1 = new Human("Gertooth");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();

        System.out.println(' ');

        Flyable[] flyablesObjects = { bat1, bird1, plane1 };
        for (int i = 0; i < flyablesObjects.length; i++) {
            flyablesObjects[i].takoff();
            flyablesObjects[i].fly();
            flyablesObjects[i].landing();
        }

        System.out.println(' ');


        Crawlable[] crawlableObjects = { snake1, crocodile1 };
        for (int i = 0; i < crawlableObjects.length; i++) {
            crawlableObjects[i].craw();
        }

        System.out.println(' ');


        Swimable [] swimableOfjects = {fish1,submarine1,crocodile1};
        for (int i = 0; i < swimableOfjects.length; i++){
            swimableOfjects[i].swim();
        }

        System.out.println(' ');

        
        Walkable [] walkableOfjects = {rat1,cat1,dog1,human1};
        for (int i = 0; i < walkableOfjects.length; i++){
            walkableOfjects[i].walk();
            walkableOfjects[i].run();
        }
    }
}
